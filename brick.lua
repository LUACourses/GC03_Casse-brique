-- Objet représentant une brique
-- reprend les caractéristiques de la classe actor (objet)
-- TODO : utiliser un héritage de la classe actor (objet)
function newBrick (level) 
  if (assertEqual(level, nil, "brick.newBrick: level")) then level = 1 end
  
  local brickColors = {
    -- dégradé de couleur pour les 20 niveaux (croissants) possible d'un brique
    {187, 255, 0},
    {212, 255, 0},
    {238, 255, 0},
    {255, 246, 0},
    {255, 220, 0},
    {255, 195, 0},
    {255, 170, 0},
    {255, 148, 0},
    {255, 123, 0},
    {255, 97,  0},
    {255, 72,  0},
    {255, 46,  0},
    {255, 21,  0},
    {167, 0,   87},
    {130, 0,   124},
    {87,  0,   167},
    {43,  0,   211},
    {0,   174, 255},
    {200, 200, 200},
    {255, 255, 255}
  }
  local brick = {   
    -- NOTE : dans une évolution future, il est possible que les briques puissent se déplacer en tombant d'où la présence de x et y
    -- position en X dans le viewport
    x,
    -- position en Y dans le viewport
    y,
    -- largeur
    width,
    -- hauteur
    height,
    -- vitesse de départ
    startSpeed,  
    -- vitesse de base
    speed,
    -- vitesse en X
    velocityX, 
    -- vitesse en Y
    velocityY, 
    -- niveau
    level,
  } 
  --[[
  initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  OPTIONNEL level: niveau de la brique (plus il est élevé, plus il faut de coup pour la faire disparaître)
  ]]
  brick.initialize = function (level) 
    debugMessage ('brick.initialize '..level)
    assertEqualQuit(viewport, nil, "player.initialize: viewport")
    if (assertEqual(level, nil, "brick.newBrick: level")) then level = 1 end
    if (level < 1 ) then level = 1 end
    if (level > #brickColors) then level = #brickColors end
    brick.x          = 0
    brick.y          = 0
    brick.width      = 80
    brick.height     = 20
    brick.startSpeed = 0
    brick.speed      = brick.startSpeed
    brick.velocityX  = brick.speed
    brick.velocityY  = -brick.speed
    brick.level      = level
    brick.livesLeft  = brick.level 
  end

  -- Détruit l'objet
  brick.destroy = function ()
    local backgroundColor
    
    if (assertEqual(viewport.backgroundColor, nil, "brick.destroy: backgroundColor")) then 
      backgroundColor = {R = 0, G = 0, B = 0} 
    else 
      backgroundColor = viewport.backgroundColor
    end

    -- on efface la brique
    love.graphics.setColor(backgroundColor.R, backgroundColor.G, backgroundColor.B) 
    love.graphics.rectangle("fill", brick.x, brick.y, brick.width - 1, brick.height - 1)
  end

  --[[
  Déplace l'objet
    dt : delta time
  ]]
  brick.move = function (dt) 
    -- mouvement standard 
    brick.x = brick.x + brick.velocityX
    brick.y = brick.y + brick.velocityY
  end

  -- Vérifie les collisions de l'objet et adapte son mouvement en conséquence
  brick.collide = function () 
    -- pour le moment les briques ne peuvent entrer en contact avec rien. Seules les collision du joueur et de la balles sont prises en compte
  end

  -- Dessine l'objet
  brick.draw = function ()
    -- le niveau de la brique peut avoir changé depuis sa création
    -- on actualise la couleur en fonction de son niveau de vie
    local colorIndex

    if (brick.livesLeft > 0 and brick.livesLeft <= #brickColors) then
      colorIndex= brick.livesLeft 
    else
      colorIndex = 1
    end 
    local brickColor = brickColors [colorIndex]
    love.graphics.setColor(brickColor[1], brickColor[2], brickColor[3])
    love.graphics.rectangle("fill", brick.x + 1, brick.y + 1, brick.width - 2, brick.height - 2, 2, 2)
  end

  -- initialisation par défaut
  brick.initialize(level)
  return brick
end --function

