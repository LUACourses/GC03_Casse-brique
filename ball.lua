-- Objet représentant la balle (la balle)
-- reprend les caractéristiques de la classe actor (objet)
-- TODO : utiliser un héritage de la classe actor (objet)
function newBall ()
  local ball = {
    -- position en X dans le viewport
    x,
    -- position en Y dans le viewport
    y,
    -- largeur
    width,
    -- hauteur
    height,
    -- vitesse de départ
    startSpeed,  
    -- vitesse de base
    speed,
    -- vitesse en X
    velocityX, 
    -- vitesse en Y
    velocityY, 
    -- couleur d'affichage
    color,
    -- TRUE si la balle est collée au joueur (au départ)
    isStuckOnPlayer
  }
  --[[
  initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  OPTIONNEL level: niveau du jeu (plus il est élevé, plus la balle ira vite)
  ]]
  ball.initialize = function (level)
    if (level == nil) then level=1 end
    debugMessage ('ball.initialize')
    ball.x                = 0
    ball.y                = 0
    ball.width            = 10
    ball.height           = 10
    ball.startSpeed       = 300 -- si on tient compte du delta time, la vitesse correspond au nombre de pixels par seconde
    ball.speed            = ball.startSpeed + level * 10
    ball.velocityX        = ball.speed
    ball.velocityY        = -ball.speed
    ball.color            = {R=238, G=255, B=0} -- rouge
    ball.isStuckOnPlayer  = true
  end

  -- Détruit l'objet
  ball.destroy = function ()
    -- on efface la brique
    love.graphics.setColor(viewport.backgroundColor.R, viewport.backgroundColor.G, viewport.backgroundColor.B) 
    love.graphics.circle ("fill", ball.x, ball.y, ball.width)
  end

  --[[
  Déplace l'objet
    dt : delta time
  ]]
  ball.move = function (dt)
    assertEqualQuit(viewport, nil, "ball.move: player", true)
    if (ball.isStuckOnPlayer) then 
      -- balle collée, on suit les déplacement du joueur
      ball.x  = player.x + (player.width - ball.width) / 2
      ball.y  = player.y - ball.width
    else 
      -- sinon on ajoute les vitesses de déplacement X et Y
      ball.x = ball.x + ball.velocityX * dt
      ball.y = ball.y + ball.velocityY * dt
    end
  end

  -- Vérifie les collisions de l'objet et adapte son mouvement en conséquence
  ball.collide = function ()
    assertEqualQuit(viewport, nil, "ball.collide: viewport", true)

    -- on fait rebondir la balle sur les bords l'écran
    local collisionXmin = viewport.Xmin + ball.width
    local collisionXmax = viewport.Xmax - ball.width
    local collisionYmin = viewport.Ymin + ball.height
    local collisionYmax = viewport.Ymax - ball.height
    if (ball.x < collisionXmin ) then 
      ball.velocityX = ball.speed
      ball.x = collisionXmin
      sndBall:play()
    else
      if (ball.x > collisionXmax) then 
        ball.velocityX = -ball.speed
        ball.x = collisionXmax  
        sndBall:play()
      end
    end
    if (ball.y < collisionYmin ) then 
      ball.velocityY = ball.speed
      ball.y = collisionYmin 
      sndBall:play()
    else
        if (ball.y > collisionYmax) then 
        -- la balle touche le bas de l'écran
        -- le bas est il en mode collision ? 
        if (gameState.isBottomCollision ) then
          -- oui, la balle rebondit
          ball.velocityY = -ball.speed
          ball.y = collisionYmax 
          sndBall:play()  
        else
          -- non, on perd la balle
          loseLife()
        end 
      end 
    end 

    -- on fait rebondir la balle si elle rentre "virtuellement" dans la raquette
    collisionXmin = player.x - ball.width / 2
    collisionXmax = player.x + player.width + ball.width / 2
    collisionYmin = player.y - ball.height / 2
    collisionYmax = player.y + player.height + ball.height / 2
    local isCollisionX = (ball.x > collisionXmin and ball.x < collisionXmax)
    local isCollisionY = (ball.y > collisionYmin and ball.y < collisionYmax)
    if (isCollisionX and isCollisionY) then
      -- on replace la balle sur les bord du paddle
      --if (ball.x > collisionXmin ) then ball.x = collisionXmin else ball.x = collisionXmax end
      if (ball.y > collisionYmin and ball.velocityY > 0) then ball.y = collisionYmin end
      if (ball.y < collisionYmax and ball.velocityY < 0) then ball.y = collisionYmax end

      -- on modifie la vitesse en X en fonction du point d'impact sur la raquette
      -- position médiane en X
      local referenceX = player.x + player.width / 2
      local offsetX = (ball.x - referenceX)
      local coeffX =  offsetX / (player.width / 2)
      --debugInfos = "referenceX:"..referenceX.." offsetX:"..offsetX.." coeffX:"..coeffX

      -- on modifie la vitesse en X de la balle
      ball.velocityX = ball.speed * coeffX 
      -- on inverse la vitesse en Y de la balle
      ball.velocityY = -ball.velocityY 
      sndBall:play()
    end

    -- collision de la balle avec les briques
    local ballRow = math.floor (ball.y / brickWall.brickTemplate.height) + 1
    local ballCol = math.floor (ball.x / brickWall.brickTemplate.width) + 1
    local ballBrick = {}
    ballBrick =  brickWall.getBrick(ballRow, ballCol)
    if (ballBrick ~= nil) then
      -- l'emplacement ou est la balle contient bien une brique
      player.score = player.score + (ballBrick.level * player.level)
      ballBrick.livesLeft = ballBrick.livesLeft - 1
      -- on inverse la vitesse en Y de la balle
      ball.velocityY = -ball.velocityY 
      if (ballBrick.livesLeft <= 0) then
        sndDestroyBrick:play()
        brickWall.deleteBrick(ballRow, ballCol)
        if (brickWall.bricksLeft <= 0 ) then
          -- tout le mur est détruit, on passe au niveau suivant
          nextLevel() 
        end
      else
        sndTouchBrick:play()
        ballBrick.draw()
      end
    end
  end

  -- Dessine l'objet
  ball.draw = function ()
    love.graphics.setColor(ball.color.R, ball.color.G, ball.color.B)
    love.graphics.circle ("fill", ball.x, ball.y, ball.width)
  end

  -- initialisation par défaut
  ball.initialize(1)
  return ball
end --function
 