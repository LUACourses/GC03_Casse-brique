-- paramètres de l'application
function newSettings () 
    local settings = {
      -- mapping clavier
      keys = {}
    }
    -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
    settings.initialize = function ()
      -- permet de ne pas toucher au code si l'on veut changer les touches utilisées par l'application
      debugMessage ('settings.initialize')
      settings.keys.quitGame      = "escape"
      settings.keys.pauseGame     = "p"
      settings.keys.startGame     = "r"
      settings.keys.previousMusic = "f1"
      settings.keys.nextMusic     = "f2"
    end

    -- initialisation par défaut
    settings.initialize()
    return settings
end --function
