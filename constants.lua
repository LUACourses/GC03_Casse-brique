-- Est-on en mode debug (affiche des infos supplémentaires) ?
DEBUG_MODE = true
-- Titre
APP_TITLE = "Casse-briques" 
-- Chemin de l'image utilisée comme icône de l'application
APP_ICON = "images/icon.png"
-- Largeur fenêtre de l'application
APP_WINDOWS_WIDTH = 800 
-- Hauteur fenêtre  de l'application
APP_WINDOWS_HEIGHT = 600
-- Nombre minimum de lignes sans briques en bas de l'écran 
EMPTY_LINES = 6
