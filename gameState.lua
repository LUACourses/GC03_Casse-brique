-- Objet représentant l'état du jeu à un instant donné
function newGameState ()
  local gameState = {
    -- le jeu est-il terminé ?
    gameHasEnded, 
    -- le jeu est-il en pause ?
    gameHasPaused,
    -- le jeu a t'il démarré ?
    gameHasStarted,
    -- le bas génère t'il une collision ou pas (mode normal = false, mode debug = true)
    isBottomCollision, 
    -- probabilité d'apparition d'une brique dans le mur (entre 0 et 1)
    brickProbability,
    -- nombre de niveau à atteindre pour gagner le jeu
    levelToWin
  }
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  gameState.initialize = function ()
    debugMessage ('gameState.initialize')
    gameState.gameHasEnded      = false
    gameState.gameHasPaused     = false
    gameState.gameHasStarted    = false
    gameState.isBottomCollision = false  
    gameState.levelToWin        = 20 
    gameState.brickProbability  = 0.8 
  end 

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  gameState.clean = function ()
    -- pour le moment rien à faire
  end

  -- initialisation par défaut
  gameState.initialize()
  return gameState
end --function