   -- Entité représentant l'affichage
function newHUD ()
  local HUD = {
  }
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  HUD.initialize = function ()
    debugMessage ('HUD.initialize')
  end 

  -- Affiche les info du jeu (HUD)
  HUD.draw = function ()
    assertEqualQuit(settings, nil, "HUD.draw: settings", true)
    assertEqualQuit(gameState, nil, "HUD.draw: gameState", true)
    assertEqualQuit(viewport, nil, "HUD.draw: viewport", true)
    assertEqualQuit(player, nil, "HUD.draw: player", true)

    local message 
    if (gameState.gameHasEnded == true) then
      message =          'Appuyer sur "'..settings.keys.quitGame ..'" pour quitter le jeu\n'
      message = message..'Appuyer sur "'..settings.keys.startGame..'" pour rejouer\n'
      if (player.hasWon == true) then
        message = "Bravo, Vous avez gagné !\n"..message
        love.graphics.setColor(0, 0, 200) -- bleu foncé
      else  
        message = "Dommage, Vous avez perdu !\n"..message
        love.graphics.setColor(200, 0, 0) -- rouge foncé
      end 
      message = message.."votre score est de "..player.score
      love.graphics.print(message, viewport.Xmax / 2, viewport.Ymax / 2)
    else 
      local i,liveImgWidth,liveImgHeight,liveImg
      liveImg = love.graphics.newImage(player.liveImagePath)
      liveImgWidth,liveImgHeight = liveImg:getDimensions()

      love.graphics.setColor(viewport.textColor.R, viewport.textColor.G, viewport.textColor.B)
      for i = 0, player.livesLeft-1 do
        love.graphics.draw(liveImg, (liveImgWidth + 5) * i, viewport.Ymax - liveImgHeight, 0, 1, 1, 0, 0)
      end

      love.graphics.setColor(255, 160, 0) -- orange
      love.graphics.print("Score "..player.score,  viewport.charWidth * 70, viewport.Ymax - viewport.charHeight)
      love.graphics.print("Niveau ", viewport.charWidth * 20, viewport.Ymax - viewport.charHeight)
      local drawX = viewport.charWidth * 25
      local drawY = viewport.Ymax - viewport.charHeight
      for i = 0, gameState.levelToWin - 1 do
        if ( i < player.level) then 
          love.graphics.setColor(200, 0, 0) 
        else
          love.graphics.setColor(100, 100, 100) -- gris
        end
        love.graphics.rectangle("fill", drawX + (i * viewport.charWidth), drawY, viewport.charWidth - 1, viewport.charHeight, 2, 2)
      end
    end
    -- affichage des infos de debug (en bas de l'écran)
    if (DEBUG_MODE and debugInfos ~='' ) then 
      love.graphics.setColor(0, 200, 0) -- vert foncé
      love.graphics.print(debugInfos, 0, 0)
    end 
  end  

  -- initialisation par défaut
  HUD.initialize()
  return HUD
end --function