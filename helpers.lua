--[[
Affiche le contenu d'une table
  var : variable à afficher
]]
function dump (var)
  if (var == nil) then 
    print "NIL"
  else
    for k, v in pairs (var) do
      print(tostring(k).." = "..tostring(v))
    end
  end
end

--[[
Retourne TRUE si une une variable est égale à une valeur donnée, FALSE sinon et affiche un message optionnel
  var: variable à tester
  value: valeur à comparer
  OPTIONNEL message: texte à afficher quand l'assertion est VRAIE
]]
function assertEqual (var, value, message)
  if (var == value) then 
    if (message ~= nil) then 
      print ("assertEqual: "..message.." Value:"..tostring(value))
    end
    return true
  else
    return false 
  end
end

--[[
Quitte l'application si une une variable est égale à une valeur donnée, et affiche un message optionnel
  var: variable à tester
  value: valeur à comparer
  OPTIONNEL message: texte à afficher quand l'assertion est VRAIE
]]
function assertEqualQuit (var, value, message) 
  if (var == value) then 
    if (message ~= nil) then 
       print ("assertEqualQuit: "..message.." Value:"..tostring(value))
    end
    print ("FIN DE L'APPLICATION SUR VALIDATION D'UNE ASSERTION")
    love.event.push('quit') 
  end 
end

--[[
Logue (Affiche) un message d'erreur
  message: texte à loguer
  OPTIONNEL mustQuit: si vrai, termine l'application après avoir afficher le message
]]
function logError (message, mustQuit) 
  print (message) 
  if (mustQuit) then love.window.close() end
end

--[[
Logue (Affiche) un message de debug
  message: texte à loguer
]]
function debugMessage (message) 
  if (DEBUG_MODE) then 
    print (message) 
  end 
end

--[[
Logue (Affiche) un message d'information
  message: texte à loguer
]]
function logMessage (message) 
  print (message) 
end