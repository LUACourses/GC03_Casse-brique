-- Entité représentant l'affichage
function newMusicManager ()
  local musicManager = {
    -- pas pour l'augmentation automatique du volume
    stepUp,
    -- pas pour la diminution automatique du volume
    stepDown,
    -- index de la piste en cours
    trackPlayed,
    -- une piste est elle en cours de lecture
    isPlaying,
    -- liste des pistes (musiques) disponibles
    trackList
  }
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  musicManager.initialize = function ()
    debugMessage ('musicManager.initialize')
    musicManager.stepUp    = 0.2 -- 1/5 de seconde
    musicManager.stepDown  = 0.2 -- 1/5 de seconde
    musicManager.trackPlayed   = 0 -- pas de morceau en cours
    musicManager.isPlaying = false
    musicManager.trackList = {}
  end 

  --[[
  Met à jour les pistes à lire et applique un fading sur le volume si besoin
    dt : delta time
  ]]
  musicManager.update = function (dt)
    if (musicManager.trackPlayed < 1 or #musicManager.trackList < 1 or not musicManager.isPlaying) then return end
    local i
    local debug = ""
    for i = 1, #musicManager.trackList do
      -- on augmente le volume de la piste en cours s'il n'est pas déjà au maximum
      local track = musicManager.trackList[i]
      local volume = track.audioSource:getVolume()
      debug = debug .. "track "..i
      if (i == musicManager.trackPlayed and volume < track.maximalVolume ) then
        track.audioSource:setVolume(volume + musicManager.stepUp * dt)
        debug = debug .. " UP "..track.audioSource:getVolume()        
      end
      -- on diminue le volume des pistes autres que celle en cours si leur volume n'est pas déjà nul
      if (i ~= musicManager.trackPlayed) then 
        if ( volume > 0 ) then
          -- on baisse le volume progressivement
          track.audioSource:setVolume(volume - musicManager.stepDown * dt)
          debug = debug .. " DW "..track.audioSource:getVolume() 
        else 
          -- volume égal à 0, on arrête la lecture
          track.audioSource:stop()
        end
      end            
    end
  end  

  --[[
  Ajoute une piste à la liste
    filename : nom complet du fichier audio (relatif au dossier de l'application)
    OPTIONNEL mode: "static" ou "stream" (voir l'option associée de la fonction love.audio.newSource)
    OPTIONNEL maximalVolume: volume maximal auquel il doit être joué (entre 0 et 1)
    OPTIONNEL isLooping: true pour jouer la piste en boucle
    OPTIONNEL noFade: true pour indiquer que cette piste sera toujours jouée immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  ]]
  musicManager.add = function (filename, mode, maximalVolume, isLooping, noFade)
    if (assertEqual(filename, nil, "musicManager.addTrack: filename")) then return end
    if (mode == nil) then mode = "static" end
    if (maximalVolume == nil) then maximalVolume = 1 end
    if (noFade == nil) then noFade = false end
    if (isLooping == nil) then isLooping = true end

    local newTrack = {}
    newTrack.audioSource = love.audio.newSource(filename,mode)
    newTrack.mode = mode
    newTrack.maximalVolume = maximalVolume
    newTrack.isLooping = isLooping
    newTrack.noFade = noFade
    table.insert(musicManager.trackList,newTrack)
  end

  --[[
  Lit la piste demandée
    trackIndex : index de la piste parmi celles ajoutées précédemment
    OPTIONNEL volume: volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
    OPTIONNEL noFade: true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  ]]
  musicManager.play = function (trackIndex, volume, nofade)
    debugMessage ('musicManager.play '..trackIndex)
    if (#musicManager.trackList < 1 ) then return end

    if (trackIndex == nil) then trackIndex = musicManager.trackPlayed end
    if (trackIndex > #musicManager.trackList) then trackIndex = #musicManager.trackList end

    local track = musicManager.trackList[trackIndex]
    if (volume ~= nil) then 
      -- si le volume a été fixé, alors on change le volume maximal de la piste 
      track.maximalVolume = volume
    end
    track.audioSource:setLooping(track.isLooping)
    if (nofade) then
      -- si la lecture est immédiate (nofade), on fixe le volumes des pistes sans attendre l'update
      musicManager.trackPlayed.audioSource:setVolume(0) -- courante à 0 
      track.audioSource:setVolume(volume) -- nouvelle au max
    else
      -- sinon on débute avec un volume = 0
      track.audioSource:setVolume(0)
    end
    musicManager.trackPlayed = trackIndex
    track.audioSource:play() 
    
    musicManager.isPlaying = true
  end

  -- Met la piste courante en pause
  musicManager.pause = function ()
    debugMessage ('musicManager.pause')
    if (#musicManager.trackList < 1 or musicManager.trackPlayed < 1) then return end
    local track = musicManager.trackList[musicManager.trackPlayed]
    if (track ~= nil) then 
      if (track.audioSource:isPlaying()) then 
        track.audioSource:pause()
      end
    end
    musicManager.isPlaying = false
  end

  -- Reprend la lecture de la piste courante
  musicManager.resume = function ()
    debugMessage ('musicManager.resume')
    if (#musicManager.trackList < 1 or musicManager.trackPlayed < 1) then return end
    local track = musicManager.trackList[musicManager.trackPlayed]
    if (track ~= nil) then 
      if (not track.audioSource:isPlaying()) then 
        track.audioSource:resume() 
      end
    end
    musicManager.isPlaying = true
  end

  -- Arrête la lecture de la piste courante
  musicManager.stop = function ()
    debugMessage ('musicManager.stop')  
    if (#musicManager.trackList < 1 or musicManager.trackPlayed < 1) then return end
    local track = musicManager.trackList[musicManager.trackPlayed]
    if (track ~= nil) then 
      track.audioSource:stop() 
    end
    musicManager.isPlaying = true
  end

  --[[
  Lit la piste suivante
    trackIndex : index de la piste parmi celles ajoutées précédemment
    OPTIONNEL volume: volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
    OPTIONNEL noFade: true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  ]]
  musicManager.playNext = function (volume, nofade)
    debugMessage ('musicManager.playNext')  
    local index = musicManager.trackPlayed + 1
    if (index > #musicManager.trackList ) then index = 1 end
    musicManager.play(index, volume, nofade)
  end

  --[[
  Lit la piste précédente
    trackIndex : index de la piste parmi celles ajoutées précédemment
    OPTIONNEL volume: volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
    OPTIONNEL noFade: true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  ]]
  musicManager.playPrevious = function (volume, nofade)
    debugMessage ('musicManager.playPrevious')  
    local index = musicManager.trackPlayed - 1
    if (index < 1 ) then index = #musicManager.trackList end
    musicManager.play(index, volume, nofade)
  end
  
  -- initialisation par défaut
  musicManager.initialize()
  return musicManager
end --function