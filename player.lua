-- Objet représentant le joueur (le paddle)
-- reprend les caractéristiques de la classe actor (objet)
-- TODO : utiliser un héritage de la classe actor (objet)
function newPlayer ()
  local player = {
    -- position en X dans le viewport
    x,
    -- position en Y dans le viewport
    y,
    -- largeur
    width,
    -- hauteur
    height,
    -- vitesse de départ
    startSpeed,  
    -- vitesse de base
    speed,
    -- vitesse en X
    velocityX, 
    -- vitesse en Y
    velocityY, 
    -- niveau
    level,
    -- couleur d'affichage
    color,
    -- chemin pour l'image représentant une vie
    liveImagePath,
    -- score
    score,
    -- nombre de vies restante
    livesLeft,
    -- le joueur a gagné la partie ?
    hasWon
  }
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  player.initialize = function()
    debugMessage ('player.initialize')
    assertEqualQuit(viewport, nil, "player.initialize: viewport", true)
    player.x          = 0
    player.y          = 0
    player.width      = 80
    player.height     = 20
    player.startSpeed = 0
    player.speed      = player.startSpeed 
    player.velocityX  = player.speed 
    player.velocityY  = -player.speed 
    player.level      = 1
    player.color      = {R=255, G=46, B=0} 
    player.liveImagePath   = 'images/player_life.png'
    player.score      = 0
    player.livesLeft  = 3
    player.hasWon     = false  
  end 

  -- Dessine l'objet
  player.draw = function ()
    love.graphics.setColor(player.color.R, player.color.G, player.color.B)
    love.graphics.rectangle("fill", player.x, player.y, player.width, player.height, 5, 5)
  end

  -- Détruit l'objet
  player.destroy = function ()
    local backgroundColor
    
    if (assertEqual(viewport.backgroundColor, nil, "player.destroy: backgroundColor")) then 
      backgroundColor = {R = 0, G = 0, B = 0} 
    else 
      backgroundColor = viewport.backgroundColor
    end

    -- on efface la brique
    love.graphics.setColor(backgroundColor.R, backgroundColor.G, backgroundColor.B)
    love.graphics.rectangle("fill", player.x, player.y, player.width, player.height)
  end

  --[[
  Déplace l'objet
    dt : delta time
  ]]
  player.move = function (dt) 
    -- déplacement du joueur avec la souris
    player.x = love.mouse.getX() - player.width / 2
    player.y = love.mouse.getY() - player.height / 2

    -- mouvement standard 
    player.x = player.x + player.velocityX
    player.y = player.y + player.velocityY
  end

  -- Vérifie les collision du joueur et adapte son mouvement en conséquence
  player.collide = function () 
    assertEqualQuit(viewport, nil, "player.collide: viewport", true)
    local collisionXmin = viewport.Xmin
    local collisionXmax = viewport.Xmax - player.width
    local collisionYmin = viewport.Ymin + player.height + brickWall.height + ball.height 
    local collisionYmax = viewport.Ymax - player.height - viewport.charHeight * 2 --le joueur ne peut pas aller sur les 2 dernières lignes, réservées à l'affichage 

    if (player.x < collisionXmin ) then 
      player.x = collisionXmin
    -- on maintien le joueur dans l'écran
    else
      if (player.x > collisionXmax) then 
        player.x = collisionXmax 
      end
    end

    if (player.y < collisionYmin ) then 
      player.y = collisionYmin
    else
      if (player.y > collisionYmax) then 
        player.y = collisionYmax 
      end
    end 
  end

  -- Place le joueur dans la zone de jeu
  player.spawn = function()
    assertEqualQuit(viewport, nil, "player.spawn: viewport", true)
    player.x = (viewport.Xmax - player.width) / 2
    player.y = viewport.Ymax - player.height - viewport.charHeight * 2 
  end 

  -- initialisation par défaut
  player.initialize()
  return player
end --function