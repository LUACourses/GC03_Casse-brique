-- TEMPLATE POUR UN Objet représentant un acteur (objet) pouvant se déplacer dans le jeu
function newActor ()
  local actor = {
    -- position en X dans le viewport
    x,
    -- position en Y dans le viewport
    y,
    -- largeur
    width,
    -- hauteur
    height,
    -- vitesse de départ
    startSpeed,  
    -- vitesse de base
    speed,
    -- vitesse en X
    velocityX, 
    -- vitesse en Y
    velocityY
  }
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  actor.initialize = function()
    debugMessage ('actor.initialize')
    assertEqualQuit(viewport, nil, "actor.initialize: viewport", true)
    actor.x          = 0
    actor.y          = 0
    actor.width      = 0
    actor.height     = 0
    actor.startSpeed = 0
    actor.startSpeed = 0
    actor.speed      = actor.startSpeed
    actor.velocityX  = actor.speed
    actor.velocityY  = -actor.speed
  end 

  -- Détruit l'objet
  actor.destroy = function ()
  end

  --[[
  Déplace l'objet
    dt : delta time
  ]]
  actor.move = function (dt) 
    -- mouvement standard 
    actor.x = actor.x + actor.velocityX
    actor.y = actor.y + actor.velocityY
  end

  -- Vérifie les collisions de l'objet et adapte son mouvement en conséquence
  actor.collide = function () 
    assertEqualQuit(viewport, nil, "actor.collide: viewport", true)
    local collisionXmin = viewport.Xmin
    local collisionXmax = viewport.Xmax - actor.width
    local collisionYmin = viewport.Ymin + actor.height
    local collisionYmax = viewport.Ymax - actor.height

    if (actor.x < collisionXmin ) then 
      actor.x = collisionXmin
    -- on maintient l'objet dans l'écran
    else
      if (actor.x > collisionXmax) then 
        actor.x = collisionXmax 
      end
    end

    if (actor.y < collisionYmin ) then 
      actor.y = collisionYmin
    else
      if (actor.y > collisionYmax) then 
        actor.y = collisionYmax 
      end
    end 
  end

  -- Dessine l'objet
  actor.draw = function ()
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  actor.clean = function ()
  end

  -- initialisation par défaut
  actor.initialize()
  return actor
end --function