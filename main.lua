require ("constants")
require ("helpers")
require ("settings")
require ("viewport")
require ("gameState")
require ("HUD")
require ("musicManager")
require ("player")
require ("ball")
require ("brickWall")

-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf("no")

-- Empêche Love de filtrer les contours des images quand elles sont redimentionnées
-- Indispensable pour du pixel art
-- love.graphics.setDefaultFilter("nearest")

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

-- variables diverses
debugInfos = ""

-- Initialiser le jeu
function initializeGame ()
  -- creation des entités
  -- Entité représentant les paramètres de l'application
  settings     = newSettings()
  -- Entité représentant la zone de jeu 
  viewport     = newViewport()
  -- Entité représentant l'état du jeu à un instant donné
  gameState    = newGameState()
   -- Entité représentant l'affichage
  HUD          = newHUD() 
   -- Entité représentant le music manager
  musicManager = newMusicManager() 
  -- Entité représentant le joueur (le paddle)
  player       = newPlayer()
  -- Entité représentant la balle (la balle)
  ball         = newBall()
  -- Entité représentant le mur de briques à détruire
  brickWall    = newBrickWall(1)

  -- charge les musiques non gérées par le musicManager (menu , écran de fin)
  musicMenu = love.audio.newSource("musics/menu_Cool.mp3","stream")
  musicEnd  = love.audio.newSource("musics/end_NodensField.mp3","stream")
  musicWin  = love.audio.newSource("musics/win_Someday.mp3","stream")
  
  -- charge les musiques gérées par le musicManager
  musicManager.add("musics/mm_ArcadeFunk.mp3","stream")
  musicManager.add("musics/mm_DefenseLine.mp3","stream")
  musicManager.add("musics/mm_NetyaginArtem13.mp3","stream")
  musicManager.add("musics/mm_NoMonkey.mp3","stream")
  musicManager.add("musics/mm_Platformer2.mp3","stream")

  -- charge les sons du jeu
  sndBall = love.audio.newSource("sounds/Ball.ogg","static")
  sndTouchBrick = love.audio.newSource("sounds/TouchBrick.ogg","static")
  sndDestroyBrick = love.audio.newSource("sounds/DestroyBrick.ogg","static")
  sndNextLevel = love.audio.newSource("sounds/NextLevel.ogg","static")
  sndLose = love.audio.newSource("sounds/LoseLife.mp3","static")

  player.spawn()
end --function

-- Lancer le jeu
function startGame ()
  debugMessage ("Lancer le jeu")
  gameState.initialize()
  player.initialize()
  ball.initialize()
  brickWall.initialize()
  player.spawn()

  brickWall.generate(player.level,gameState.brickProbability)
  musicManager.play(1)   
end --function

-- Mettre le jeu en pause
function pauseGame()
  gameState.gameHasPaused = not gameState.gameHasPaused
  debugMessage ('Jeu en pause:'..tostring(gameState.gameHasPaused))     
  if (gameState.gameHasPaused) then
    musicMenu:play()
    musicManager.pause()
  else 
    musicMenu:stop()
    musicManager.resume()
  end    
end

-- le joueur perd une "vie"
function loseLife ()
  logMessage ("Vie perdue")
  if (player.livesLeft <= 1 ) then
    player.destroy() -- juste au cas ou il y aurait une animation dans la destruction du joueur
    loseGame()
  else
    sndLose:play()
    player.livesLeft = player.livesLeft - 1
    ball.destroy() -- juste au cas ou il y aurait une animation dans la destruction de la balle
    player.spawn()
    ball.initialize()
  end
end --function

function nextLevel()
  logMessage ("Niveau terminé")
  player.level = player.level + 1
  if (player.level >= gameState.levelToWin ) then
    -- le jeu est gagné lorsque le niveau max est atteint
    winGame()
  else 
    sndNextLevel:play()
    brickWall.generate(player.level, gameState.brickProbability)
    player.spawn()
    ball.initialize(player.level)
    musicManager.playNext()
  end
end --function 

-- le joueur perd la partie
function loseGame ()
  logMessage ("Partie perdue")
  sndLose:play()
  gameState.gameHasEnded = true
  player.hasWon = false
  musicEnd:play()
  musicManager.stop()
end --function

-- le joueur gagne la partie
function winGame ()
  logMessage ("Partie gagnée")
  gameState.gameHasEnded = true
  player.hasWon = true
  musicWin:play()
  musicManager.stop()
end --function

-- Quitter le jeu
function quitGame ()
  debugMessage ("Quitter le jeu")
  gameState.clean()
  viewport.clean()
  love.event.push('quit') 
end --function

-- Love.load pour initialiser l'application
function love.load()
  --debugInfos = "Programme démarré...\n"
  love.window.setTitle(APP_TITLE) 
  local imgIcon = love.graphics.newImage(APP_ICON)
  love.window.setIcon(imgIcon:getData())
  -- Change les dimensions de la fenêtre
  love.window.setMode(APP_WINDOWS_WIDTH, APP_WINDOWS_HEIGHT) 

  -- Initialisation du random avec un temps en ms
  math.randomseed(love.timer.getTime()) 

  initializeGame()
  startGame()
end --function

-- Love.update est utilisé pour gérer l'état de l'application frame-to-frame
function love.update(dt)
  
  if (not gameState.gameHasPaused and not gameState.gameHasEnded) then
    -- déplacements et collisions du joueur
    player.move(dt)
    player.collide()

    -- déplacements et collisions de la balle
    ball.move(dt)
    ball.collide()

    -- déplacements et collisions du mur de briques
    brickWall.move(dt)
    brickWall.collide()
  end --if (not gameState.gameHasPaused and not gameState.gameHasEnded) then

  --met à jour les musiques 
  musicManager.update(dt)

  -- infos de debug
  --debugInfos =                    "joueur: X="..math.floor(player.x)    .." Y="..math.floor(player.y)    .." W="..player.width    .." H="..player.height 
  --debugInfos = debugInfos.."\n".. "balle : X="..math.floor(ball.x).." Y="..math.floor(ball.y).." W="..ball.width.." H="..ball.height 
end --function

 -- Love.draw est utilisé pour afficher l'état de l'application sur l'écran.
function love.draw()
  -- si ce n'a pas déjà été fait,on dessine le viewport
  if (not viewport.firtDrawDone) then viewport.draw() end

  if (not gameState.gameHasEnded) then
    -- Dessine le joueur (paddle)
    player.draw()

    -- Dessine la balle
    ball.draw()

    -- desine le mur de briques
    brickWall.draw()
  end 

  -- affiche les infos du jeu
  HUD.draw ()
end --function

-- intercepte l'appui des touches du clavier
function love.keypressed(key)
  debugMessage ("touche appuyée:"..key) 
  -- Quitter le jeu
  if love.keyboard.isDown (settings.keys.quitGame) then quitGame() end
  -- Jouer la piste musicale suivante
  if love.keyboard.isDown (settings.keys.nextMusic) then musicManager.playNext() end
  -- Jouer la piste musicale précédente
  if love.keyboard.isDown (settings.keys.previousMusic) then musicManager.playPrevious() end

  if (not gameState.gameHasEnded) then
    -- Mettre le jeu en pause
    if love.keyboard.isDown (settings.keys.pauseGame) then pauseGame() end
  else 
    -- Relancer le jeu 
    if love.keyboard.isDown (settings.keys.startGame) then startGame() end    
  end
end --function

-- intercepte le clic de souris
function love.mousepressed (x,y,n) 
  debugMessage ("bouton appuyé:"..n) 
  if (not gameState.gameHasEnded) then
    if (ball.isStuckOnPlayer == true) then 
      -- on lance la balle sur un clic
      ball.isStuckOnPlayer = false
    end
  end
  gameState.gameHasStarted = true 
end --function