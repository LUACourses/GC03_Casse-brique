  -- Objet représentant la zone de jeu 
function newViewport ()
  local viewport = {
    -- position X minimale 
    Xmin,
    -- position X maximale 
    Xmax,
    -- position Y minimale 
    Ymin,
    -- position Y maximale 
    Ymax,
    -- largeur de caractère
    charWidth,
    -- hauteur de caractère
    charHeight,
    -- couleur de fond par défaut
    backgroundColor,
    -- couleur de texte par défaut
    textColor,
    -- le viewport a t'il déjà été dessiné ?
    firtDrawDone 
  } 
  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  viewport.initialize = function ()
    debugMessage ('viewport.initialize')
    viewport.Xmin = 0
    viewport.Xmax = love.graphics.getWidth()
    viewport.Ymin = 0
    viewport.Ymax = love.graphics.getHeight()
    viewport.charWidth  = 10
    viewport.charHeight = 15
    viewport.backgroundColor = {R = 0, G = 0, B = 50}
    viewport.textColor = {R = 255, G = 255, B = 255}
    viewport.firtDrawDone = false
    -- cache le curseur de la souris
    love.mouse.setVisible(false)
  end
  
  -- Dessine l'objet
  viewport.draw = function ()
    -- on utilise la couleur de fond définie pour remplir la zone de jeu
    love.graphics.setColor(viewport.backgroundColor.R, viewport.backgroundColor.G, viewport.backgroundColor.B)
    love.graphics.rectangle("fill", viewport.Xmin, viewport.Ymin, viewport.Xmax - 1, viewport.Ymax - 1)
    love.graphics.setColor(viewport.textColor.R, viewport.textColor.G, viewport.textColor.B)
  end

  -- Effectue des nettoyages en quittant le jeu 
  viewport.clean = function ()
      -- affiche le curseur de la souris
    love.mouse.setVisible(true)
  end

  -- initialisation par défaut
  viewport.initialize()
  return viewport
end --function
