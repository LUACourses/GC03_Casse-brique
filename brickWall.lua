require ("brick")

-- Objet représentant un mur de briques
-- reprend les caractéristiques de la classe actor (objet)
-- TODO : utiliser un héritage de la classe actor (objet)
function newBrickWall ()
  local brickWall = {
    -- NOTE : dans une évolution future, il est possible que le mur de brique se déplace pour augmenter la difficulté d'où la présence de x et y
    -- position en X dans le viewport
    x,
    -- position en Y dans le viewport
    y,
    -- largeur en pixel en fonction du nombre de colonnes et du modèle de brique
    width,
    -- hauteur en pixel en fonction du nombre de lignes et du modèle de brique
    height,
    -- vitesse de départ
    startSpeed,  
    -- vitesse de base
    speed,
    -- vitesse en X
    velocityX, 
    -- vitesse en Y
    velocityY, 
    -- largeur exprimée en hauteur de brique
    cols,
    -- hauteur exprimée en hauteur de brique
    rows,
    -- nombre maximal de briques sur une ligne en fonction de la largeur de la zone de jeu
    colMax,
    -- nombre maximal de ligne de briques en fonction de la hauteur de la zone de jeu
    rowMax,
    -- template utilisé pour les briques du mur
    brickTemplate, 
    -- contenu du mur : pour le moment : succession de 0 et de 1, avec 0 indiquant aucune brique, 1 indiquant une brique
    content,
    -- nombre de brique restant
    bricksLeft,
    -- couleur de fond (effacement des briques)
    backgroundColor
  }

  -- Initialise l'objet avec les valeurs par défaut ou celle passées à la fonction
  brickWall.initialize = function ()
    debugMessage ('brickWall.initialize')
    assertEqualQuit(viewport, nil, " brickWall.initialize: viewport", true)
    brickWall.x          = 0
    brickWall.y          = 0
    brickWall.width      = 0
    brickWall.height     = 0
    brickWall.startSpeed = 0
    brickWall.speed      = brickWall.startSpeed
    brickWall.velocityX  = brickWall.speed
    brickWall.velocityY  = brickWall.speed

    brickWall.brickTemplate = newBrick (1)
    brickWall.cols            = 15 -- par défaut, calculé automatiquement en fonction du nombre de briques du mur
    brickWall.rows            = 6  -- par défaut, calculé automatiquement en fonction du nombre de briques du mur
    brickWall.bricksLeft      = 0  -- par défaut, calculé automatiquement en fonction du nombre de briques du mur
    -- on calcule la taille max du mur en fonction du modèle de brique  
    brickWall.colMax          = math.floor(viewport.Xmax / brickWall.brickTemplate.width)
    brickWall.rowMax          = math.floor(viewport.Ymax / brickWall.brickTemplate.height) - EMPTY_LINES 
    brickWall.content         = {}
    brickWall.backgroundColor = viewport.backgroundColor
  end --function


  --[[
  Déplace l'objet
    dt : delta time
  ]]
  brickWall.move = function (dt) 
    -- mouvement standard 
    brickWall.x = brickWall.x + brickWall.velocityX
    brickWall.y = brickWall.y + brickWall.velocityY
  end

  -- Vérifie les collisions de l'objet et adapte son mouvement en conséquence
  brickWall.collide = function () 
  end

  -- Dessine l'objet
  brickWall.draw = function ()
    local r, c, aBrick
    for r = 1, brickWall.rows do
      for c = 1,brickWall.cols do
        aBrick = brickWall.getBrick(r,c)
        if (aBrick ~= nil) then
          aBrick.x = aBrick.width * (c-1)
          aBrick.y = aBrick.height * (r-1)
          aBrick.draw()
        end
      end 
    end
  end 

  --[[
  génère le mur de briques en fonction du niveau
    OPTIONNEL level: niveau du jeu (plus il est élevé, plus le nombre et le niveau des briques est élevé)
    OPTIONNEL brickProbability: probability (entre 0 et 1) qu'il y est une brique à un emplacement du mur au lieu d'un trou
  ]]
  brickWall.generate = function (level, brickProbability)
    if (assertEqual (brickWall.brickTemplate,nil, "brickWall.brickTemplate")) then return end 

    if (level == nil ) then level = 1 end
    if (brickProbability == nil ) then brickProbability = 1 end

    local numberOfBricks = level * 10

    -- pas plus de briques que d'emplacement possibles
    local numberMax = brickWall.colMax * brickWall.rowMax 
    if (numberOfBricks > numberMax) then numberOfBricks = numberMax end
    
    if (numberOfBricks > brickWall.colMax ) then 
      -- on remplit chaque ligne au maximum
      brickWall.cols = brickWall.colMax 
      brickWall.rows = math.floor(numberOfBricks / brickWall.colMax)
    else
      brickWall.cols = numberOfBricks
      brickWall.rows = 1
    end 
    brickWall.width = brickWall.cols * brickWall.brickTemplate.width
    brickWall.height = brickWall.rows * brickWall.brickTemplate.height

    -- creation des briques
    brickWall.bricksLeft = 0
    local r,c 
    for r = 1, brickWall.rows do
      brickWall.content[r] = {}
      for c = 1, brickWall.cols do
        if (math.random(0,1) < brickProbability) then
          brickWall.content[r][c] = newBrick(math.random(1,level))
          brickWall.bricksLeft = brickWall.bricksLeft + 1
        end
      end
    end
  end --function

  --[[
  renvoi le contenu d'une case du mur: nil, ou un objet brick
    row: ligne de la case a vérifier
    col: colonne de la case a vérifier
  ]] 
  brickWall.getBrick = function (row,col) 
    local value
    if (brickWall.content ~= nil ) then 
      if (brickWall.content[row] ~= nil ) then
        if (brickWall.content[row][col] ~= nil ) then  
          value = brickWall.content[row][col]
        else 
          --logMessage ("brickWall.content["..row.."]["..col.."] est indéfini")
          value = nil
        end
      else
        --logMessage ("brickWall.content["..row.."] est indéfini")
        value = nil
      end
    else
      --logMessage ("brickWall.content est indéfini")
      value = nil
    end
    --debugMessage ("brickWall.getBrick: Brick ("..row..","..col..")="..tostring(value))
    return value
  end --function

  --[[
  supprime une brique 
    row: ligne de la case a vérifier
    col: colonne de la case a vérifier
  ]] 
  brickWall.deleteBrick = function (row,col) 
    if (brickWall.content ~= nil ) then 
      if (brickWall.content[row] ~= nil ) then
        if (brickWall.content[row][col] ~= nil ) then
          brickWall.content[row][col].destroy(brickWall.backgroundColor)
          brickWall.content[row][col] = nil
          brickWall.bricksLeft = brickWall.bricksLeft -1
        else 
          --logMessage ("brickWall.content["..row.."]["..col.."] est indéfini")
        end
      else
        --logMessage ("brickWall.content["..row.."] est indéfini")
      end
    else
      --logMessage ("brickWall.content est indéfini")
    end
  end

  -- initialisation par défaut
  brickWall.initialize ()
  return brickWall
end --function
