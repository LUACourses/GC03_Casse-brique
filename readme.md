## Casse Brique

### Description
jeu de type casse brique écrit en LUA et Love2D.  

===
### Objectifs
Détruire toutes les briques pour passer au niveau suivant.  

Le jeu comprend 20 niveaux.  
Le jeu est gagné quand le joueur a passé les 20 niveaux du jeu.  
Le jeu est perdu quand le joueur n'a plus de vie.  

### Mouvements et actions du joueur
Déplacer le joueur: mouvements de la souris.  
Lancer la balle: clic gauche.  
Mettre en pause : touche P.  
Relancer la partie: touche R.  
Musique précédente: touche F1.  
Musique suivante: touche F2.  
Quitter le jeu: clic sur la croix ou touche escape.  

### Interactions
L'angle de rebond de la balle varie en fonction de son point de contact avec le paddle.  
La vitesse de la balle augmente avec le niveau du joueur.  
Le joueur perd une vie si la balle atteint le bas de l'écran.  

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

Inspiré du cours dispensé par l'école de formation en ligne "GameCodeur", disponible sur le site http://www.gamecodeur.fr

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
Brick-break game written in LUA and Love2D.  

===
### Goals
Destroy all the bricks to move to the next level.  

The game includes 20 levels.  
The game is won when the player has passed the 20 levels of the game.  
The game is lost when the player no longer has life.  

### Movements and actions of the player
Move the player: moves the mouse.  
Throwing the ball: left click
Pause: P key.  
Restart the game: R key.  
Previous music: F1 key.  
Next music: F2 key.  
Exit the game: click on the cross or escape key.  

### Interactions
The bounce angle of the ball varies according to its point of contact with the paddle.  
The speed of the ball increases with the level of the player.  
The player loses a life if the ball reaches the bottom of the screen.  

### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

Inspired by the course given by the online training school "GameCodeur", available on the site http://www.gamecodeur.fr

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)